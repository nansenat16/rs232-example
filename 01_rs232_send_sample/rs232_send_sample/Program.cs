﻿/*
 * Created by SharpDevelop.
 * User: nansen
 * Date: 2013/11/10
 * Time: 上午 09:08
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO.Ports;
using System.Threading;

namespace rs232_send_sample
{
	class Program
	{
		public static void Main(string[] args)
		{
			Console.WriteLine("\t Data emulator");
			
			SerialPort sp=new SerialPort();
			sp.BaudRate=9600;
			sp.PortName="COM3";
			
			if(sp.IsOpen==false){
				sp.Open();
			}else{
				return;
			}
			
			Random r=new Random();
			
			byte[] data=new byte[]{0x01,0x45,0x4d,0x55,0x02,0x00,0x00,0x03,0x0a};
			while(true){
				try{
					UInt16 val=Convert.ToUInt16(r.Next(80,160));
					Console.WriteLine(" Send : "+val);
					
					//print_hex(BitConverter.GetBytes(val),"Debug Val");
					
					Array.Copy(BitConverter.GetBytes(val),0,data,5,2);
					sp.Write(data,0,data.Length);
				}catch(Exception e){
					Console.WriteLine(e.ToString());
				}finally{
					Thread.Sleep(3000);//slow down
				}
			}
		}
		
		private static void print_hex(byte[] data,string txt){
			string output="";
			foreach (Byte bi in data) {
				output += bi.ToString("X2") + ",";
			}
			output=output+Environment.NewLine+"  Len:"+data.Length+"   "+txt+"    "+DateTime.Now.ToString("HH:mm:ss")+Environment.NewLine+"---"+Environment.NewLine;
			Console.WriteLine(output);
		}
	}
}