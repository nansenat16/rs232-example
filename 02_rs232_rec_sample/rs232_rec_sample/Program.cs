﻿/*
 * Created by SharpDevelop.
 * User: worker
 * Date: 2013/11/10
 * Time: 上午 09:07
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.IO.Ports;
using System.Threading;
using System.Text;

namespace rs232_rec_sample
{
	class Program
	{
		private static SerialPort sp;
		
		public static void Main(string[] args)
		{
			Console.WriteLine("\t Data Rec");
			
			sp=new SerialPort();
			sp.BaudRate=9600;
			sp.PortName="COM4";
			
			if(sp.IsOpen==false){
				sp.DataReceived+=new SerialDataReceivedEventHandler(read_data);
				sp.Open();
			}
			
			
			while(true){//hold console program don't exit.
				Thread.Sleep(1000);
			}
		}
		
		private static void read_data(object sender,SerialDataReceivedEventArgs e){
			byte[] tmp_data=new byte[20];
            if (sp.BytesToRead < 9) { return; }
			int rec_len=sp.Read(tmp_data,0,20);
			byte[] rec_data=new byte[rec_len];
			Array.Copy(tmp_data,0,rec_data,0,rec_len);
			
			byte[] tmp_val=new byte[2];
			Array.Copy(rec_data,5,tmp_val,0,2);
			
			UInt16 val=BitConverter.ToUInt16(tmp_val,0);
			Console.WriteLine(" Val : "+val);
            print_hex(rec_data, "Rec Data");
        }
		
		private static void print_hex(byte[] data,string txt){
			string output="";
			foreach (Byte bi in data) {
				output += bi.ToString("X2") + ",";
			}
			output=output+Environment.NewLine+"  Len:"+data.Length+"   "+txt+"    "+DateTime.Now.ToString("HH:mm:ss")+Environment.NewLine+"---"+Environment.NewLine;
			Console.WriteLine(output);
		}
	}
}