﻿/*
 * Created by SharpDevelop.
 * User: worker
 * Date: 2013/11/12
 * Time: 下午 12:36
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;

using System.IO.Ports;
using System.ComponentModel;//BackgroundWorker with RS232_Rec class
using System.Diagnostics;//debug tool

namespace com.worker
{
	// You can Build these class in one dll file and load/use it.
	public class RS232_Rec{
		private SerialPort sp=null;
		private ValData RecData=new ValData();
		
		public RS232_Rec(){
			BackgroundWorker bgworker_Rec = new BackgroundWorker();
			bgworker_Rec.DoWork += new DoWorkEventHandler(bgwRS232_Rec);
			bgworker_Rec.RunWorkerAsync();
		}
		
		private void bgwRS232_Rec(object sender,DoWorkEventArgs e){
			sp=new SerialPort();
			sp.BaudRate=9600;
			sp.PortName="COM4";
			
			if(sp.IsOpen==false){
				sp.DataReceived+=new SerialDataReceivedEventHandler(read_data);
				sp.Open();
			}
		}
		
		private void read_data(object sender,SerialDataReceivedEventArgs e){
			byte[] tmp_data=new byte[20];
            if (sp.BytesToRead < 9) { return; }
            int rec_len=sp.Read(tmp_data,0,20);
			byte[] rec_data=new byte[rec_len];
			Array.Copy(tmp_data,0,rec_data,0,rec_len);
			
			byte[] tmp_val=new byte[2];
			Array.Copy(rec_data,5,tmp_val,0,2);
			
			UInt16 val=BitConverter.ToUInt16(tmp_val,0);
			//Debug.WriteLine(" Val : "+val);
			this.RecData.val=val;
			this.DoRecEvent();
		}
		
		public delegate void OnRecEvent(object sender,RecEvent e);
		public event OnRecEvent OnRecData;
		private void DoRecEvent(){
			if(this.OnRecData!=null){
				this.OnRecData(this,new RecEvent(this.RecData));
			}
		}
	}
	
	public class RecEvent :EventArgs{
		public ValData data{get;private set;}
		public RecEvent(ValData data){
			this.data=data;
		}
	}
	
	public class ValData{
		public uint val;
	}
}