﻿/*
 * Created by SharpDevelop.
 * User: worker
 * Date: 2013/11/12
 * Time: 下午 12:42
 * 
 * To change this template use Tools | Options | Coding | Edit Standard Headers.
 */
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;
using com.worker;

namespace com.example
{
	/// <summary>
	/// Description of MainForm.
	/// </summary>
	public partial class MainForm : Form
	{
		public MainForm()
		{
			//
			// The InitializeComponent() call is required for Windows Forms designer support.
			//
			InitializeComponent();
			
			//
			// TODO: Add constructor code after the InitializeComponent() call.
			//
			
			RS232_Rec rec=new RS232_Rec();
			rec.OnRecData+= new RS232_Rec.OnRecEvent(rec_OnRecData);
		}
		
		private void rec_OnRecData(object sender,RecEvent e){
			//Debug.WriteLine(e.data.val);

			// Can NOT Do This !!
			//valbox.Text=e.data.val.ToString();
			
			set_Val(e.data.val.ToString(),valbox);
		}
		
		private delegate void use_Val(string val,TextBox ui);
		
		private void set_Val(string val,TextBox ui){
			if(this.InvokeRequired){
				use_Val use=new use_Val(set_Val);
				this.Invoke(use,val,ui);
			}else{
				ui.Text=val;
			}
		}
		
	}
}
