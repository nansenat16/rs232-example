﻿using System;
using System.Windows.Forms;
using System.IO.Ports;

namespace _06_BP_Rec
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private SerialPort SP;

        private void Form1_Load(object sender, EventArgs e)
        {
            try
            {
                SP = new SerialPort();
                SP.BaudRate = 4800;
                SP.PortName = "COM4";
                SP.DataBits = 8;
                SP.Parity = Parity.None;
                SP.StopBits = StopBits.One;
                SP.DataReceived += SP_DataReceived;
                SP.Open();

            }
            catch (Exception exp) {
                val_msg.Text = exp.Message;
            }
        }

        private void SetDataVal(string sys, string dia, string hr) {
            this.Invoke((MethodInvoker)delegate ()
            {
                val_Sys.Text = sys;
                val_Dia.Text = dia;
                val_HR.Text = hr;
            });
        }

        string buffer = "";
        private void SP_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            buffer += SP.ReadExisting();
            if (buffer.Split(' ').Length < 6 || buffer.Length < 30) { return; }

            String[] getData = buffer.Split(' ');
            string sys= getData[3];
            string dia=getData[4];
            string hr= getData[5].Substring(0, 3);
            SetDataVal(sys, dia, hr);

            buffer = "";
        }
    }
}
