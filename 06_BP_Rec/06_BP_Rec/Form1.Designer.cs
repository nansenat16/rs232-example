﻿namespace _06_BP_Rec
{
    partial class Form1
    {
        /// <summary>
        /// 設計工具所需的變數。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清除任何使用中的資源。
        /// </summary>
        /// <param name="disposing">如果應該處置 Managed 資源則為 true，否則為 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 設計工具產生的程式碼

        /// <summary>
        /// 此為設計工具支援所需的方法 - 請勿使用程式碼編輯器修改
        /// 這個方法的內容。
        /// </summary>
        private void InitializeComponent()
        {
            this.val_msg = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.val_Sys = new System.Windows.Forms.Label();
            this.val_Dia = new System.Windows.Forms.Label();
            this.val_HR = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // val_msg
            // 
            this.val_msg.Font = new System.Drawing.Font("新細明體", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.val_msg.Location = new System.Drawing.Point(13, 13);
            this.val_msg.Name = "val_msg";
            this.val_msg.Size = new System.Drawing.Size(359, 45);
            this.val_msg.TabIndex = 0;
            this.val_msg.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label1.Location = new System.Drawing.Point(18, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(106, 60);
            this.label1.TabIndex = 1;
            this.label1.Text = "Sys";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label2.Location = new System.Drawing.Point(18, 141);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(107, 60);
            this.label2.TabIndex = 2;
            this.label2.Text = "Dia";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.label3.Location = new System.Drawing.Point(18, 226);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(104, 60);
            this.label3.TabIndex = 3;
            this.label3.Text = "HR";
            // 
            // val_Sys
            // 
            this.val_Sys.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.val_Sys.Location = new System.Drawing.Point(144, 62);
            this.val_Sys.Name = "val_Sys";
            this.val_Sys.Size = new System.Drawing.Size(228, 60);
            this.val_Sys.TabIndex = 4;
            this.val_Sys.Text = "----";
            // 
            // val_Dia
            // 
            this.val_Dia.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.val_Dia.Location = new System.Drawing.Point(144, 141);
            this.val_Dia.Name = "val_Dia";
            this.val_Dia.Size = new System.Drawing.Size(228, 60);
            this.val_Dia.TabIndex = 5;
            this.val_Dia.Text = "----";
            // 
            // val_HR
            // 
            this.val_HR.Font = new System.Drawing.Font("新細明體", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(136)));
            this.val_HR.Location = new System.Drawing.Point(144, 226);
            this.val_HR.Name = "val_HR";
            this.val_HR.Size = new System.Drawing.Size(228, 60);
            this.val_HR.TabIndex = 6;
            this.val_HR.Text = "----";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(385, 326);
            this.Controls.Add(this.val_HR);
            this.Controls.Add(this.val_Dia);
            this.Controls.Add(this.val_Sys);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.val_msg);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label val_msg;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label val_Sys;
        private System.Windows.Forms.Label val_Dia;
        private System.Windows.Forms.Label val_HR;
    }
}

